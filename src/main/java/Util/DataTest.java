package Util;

import com.github.javafaker.Faker;

import java.util.Locale;

public class DataTest {

    private Locale local = new Locale("en");
    private Faker faker = new Faker(local);

    public static DataTest getData() {
        return new DataTest();
    }

    public String getFirstName() {
        return faker.address().firstName();
    }
    public String getLastName() {
        return faker.address().lastName();
    }
    public String getEmail() {
        return faker.internet().emailAddress();
    }
    public String getPassword() {
        return faker.internet().password(8, 10);
    }

}
