package PageObject;

import Core.BasePage;
import Core.PageObjectManage;
import PageUI.HomePageUI;
import org.openqa.selenium.WebDriver;

public class HomePageObject extends BasePage {

    private WebDriver driver;

    public HomePageObject(WebDriver driver){
        this.driver = driver;
    }

    public RegisterPageObject OpenRegisterPage(){
        waitUntilElementClickAble(driver, HomePageUI.ACCOUNT_LINK);
        clickButton(driver, HomePageUI.ACCOUNT_LINK);
        waitUntilElementClickAble(driver, HomePageUI.REGISTER_LINK);
        clickButton(driver, HomePageUI.REGISTER_LINK);
        return PageObjectManage.getRegisterPage(driver);
    }

    public LoginPageObject OpenLoginPage(){
        waitUntilElementClickAble(driver, HomePageUI.ACCOUNT_LINK);
        clickButton(driver, HomePageUI.ACCOUNT_LINK);
        waitUntilElementClickAble(driver, HomePageUI.LOGIN_LINK);
        clickButton(driver, HomePageUI.LOGIN_LINK);
        return PageObjectManage.getLoginPage(driver);
    }


}
