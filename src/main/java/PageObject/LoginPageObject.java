package PageObject;

import Core.BasePage;
import Core.PageObjectManage;
import PageUI.LoginPageUI;
import PageUI.RegisterPageUI;
import org.openqa.selenium.WebDriver;

public class LoginPageObject extends BasePage {

    private WebDriver driver;

    public LoginPageObject(WebDriver driver){
        this.driver = driver;
    }

    public MyDashBoardPageObject login(String email, String password){
        waitUntilElemenatVisible(driver, LoginPageUI.EMAIL_FIELD);
        inputText(driver, LoginPageUI.EMAIL_FIELD, email);
        inputText(driver, LoginPageUI.PASSWORD_FIELD, password);
        clickButton(driver, LoginPageUI.LOGIN_BTN);
        return PageObjectManage.getDashBoardPage(driver);
    }

}
