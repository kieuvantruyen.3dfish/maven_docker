package PageObject;

import Core.BasePage;
import PageUI.MyDashBoardPageUI;
import org.openqa.selenium.WebDriver;
import static org.assertj.core.api.Assertions.assertThat;

public class MyDashBoardPageObject extends BasePage {

    private WebDriver driver;

    public MyDashBoardPageObject(WebDriver driver){
        this.driver = driver;
    }

    public void CheckPageTitleDisplays(){
        waitUntilElemenatVisible(driver, MyDashBoardPageUI.TITLE);
        assertThat(isDisplay(driver, MyDashBoardPageUI.TITLE)).isTrue();
    }

}
