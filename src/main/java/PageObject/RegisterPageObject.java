package PageObject;

import Core.BasePage;
import Core.PageObjectManage;
import PageUI.RegisterPageUI;
import org.openqa.selenium.WebDriver;

public class RegisterPageObject extends BasePage {

    private WebDriver driver;

    public RegisterPageObject(WebDriver driver){
        this.driver = driver;
    }

    public void inputFirstName(String firstName){
        waitUntilElemenatVisible(driver, RegisterPageUI.FIRST_NAME_FIELD);
        inputText(driver, RegisterPageUI.FIRST_NAME_FIELD, firstName);
    }

    public void inputLastName(String lastName){
        waitUntilElemenatVisible(driver, RegisterPageUI.FIRST_NAME_FIELD);
        inputText(driver, RegisterPageUI.LAST_NAME_FIELD, lastName);
    }

    public void inputEmail(String email){
        waitUntilElemenatVisible(driver, RegisterPageUI.EMAI_FIELD);
        inputText(driver, RegisterPageUI.EMAI_FIELD, email);
    }

    public void inputPassword(String password){
        waitUntilElemenatVisible(driver, RegisterPageUI.PASSWORD_FIELD);
        inputText(driver, RegisterPageUI.PASSWORD_FIELD, password);
    }

    public void inputConfirmPassword(String password){
        waitUntilElemenatVisible(driver, RegisterPageUI.CONFIRM_PASS_FIELD);
        inputText(driver, RegisterPageUI.CONFIRM_PASS_FIELD, password);
    }

    public MyDashBoardPageObject clickRegisterButton(){
        waitUntilElementClickAble(driver, RegisterPageUI.REGISTER_BTN);
        clickButton(driver, RegisterPageUI.REGISTER_BTN);
        return PageObjectManage.getDashBoardPage(driver);
    }
}
