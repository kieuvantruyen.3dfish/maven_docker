package PageUI;

public class RegisterPageUI {
    public static final String FIRST_NAME_FIELD = "//input[@id='firstname']";
    public static final String LAST_NAME_FIELD = "//input[@id='lastname']";
    public static final String EMAI_FIELD = "//input[@id='email_address']";
    public static final String PASSWORD_FIELD = "//input[@id='password']";
    public static final String CONFIRM_PASS_FIELD = "//input[@id='confirmation']";
    public static final String REGISTER_BTN = "//button[@title='Register']";
}
