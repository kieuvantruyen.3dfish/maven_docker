package PageUI;

public class LoginPageUI {
    public static final String EMAIL_FIELD = "//input[@id='email']";
    public static final String PASSWORD_FIELD = "//input[@id='pass']";
    public static final String LOGIN_BTN = "//button[@title='Login']";
}
