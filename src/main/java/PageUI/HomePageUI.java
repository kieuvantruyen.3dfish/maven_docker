package PageUI;

public class HomePageUI {
    public static final String ACCOUNT_LINK = "//div[@class='account-cart-wrapper']//span[contains(text(),'Account')]";
    public static final String REGISTER_LINK = "//ul/li[5]";
    public static final String LOGIN_LINK = "//ul/li[6]";
}
