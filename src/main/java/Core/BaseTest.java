package Core;

import Util.GlobalContants;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    private WebDriver driver;

    protected WebDriver getDriver(String browser, String url){
        if(browser.equals("chrome")){
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else if(browser.equals("firefox")){
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        }
        driver.get(url);
        return driver;
    }

    protected WebDriver getDriverGrid(String browser, String url, String ip, String port) throws MalformedURLException {
        DesiredCapabilities capabilities = null;
        if(browser.equals("chrome")){
            capabilities = DesiredCapabilities.chrome();
        } else if(browser.equals("firefox")){
            capabilities = DesiredCapabilities.firefox();
        }
        driver = new RemoteWebDriver(new URL(String.format("http://%s:%S/wd/hub",ip, port)), capabilities);
        driver.manage().window().maximize();
        driver.get(url);
        return driver;
    }

    protected WebDriver getDriverBrowserStack(String url, String osName, String osVersion, String browser, String browserVersion){
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("os", osName);
        capabilities.setCapability("os_version",osVersion);
        capabilities.setCapability("browser",browser);
        capabilities.setCapability("browser_version",browserVersion);
        capabilities.setCapability("browserstack.debug","true");
        capabilities.setCapability("browserstack.selenium_version","3.14.0");
        capabilities.setCapability("name","Run on" + osName + " and " +browser + "with version "+browserVersion);

        try {
            driver = new RemoteWebDriver(new URL(GlobalContants.BROWSER_URL), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(url);
        driver.manage().window().maximize();
        return  driver;
    }

    protected WebDriver getDriverSauceLab(String url, String osName,  String browser, String browserVersion){
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("os", osName);
        capabilities.setCapability("browserName",browser);
        capabilities.setCapability("browserVersion",browserVersion);
        capabilities.setCapability("name","Run on" + osName + " and " +browser + "with version "+browserVersion);
        try {
            driver = new RemoteWebDriver(new URL(GlobalContants.SAUCELAB_URL), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(url);
        driver.manage().window().maximize();
        return  driver;
    }

    protected WebDriver getDriverDocker(String browser, String url) {
        DesiredCapabilities capabilities = null;
        if(browser.equals("chrome")){
            capabilities = DesiredCapabilities.chrome();
        } else if(browser.equals("firefox")){
            capabilities = DesiredCapabilities.firefox();
        }
        try {
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(url);
        driver.manage().window().maximize();
        return driver;
    }
}
