package Core;

import PageObject.HomePageObject;
import PageObject.LoginPageObject;
import PageObject.MyDashBoardPageObject;
import PageObject.RegisterPageObject;
import org.openqa.selenium.WebDriver;

public class PageObjectManage {
    public static HomePageObject getHomePage(WebDriver driver){
        return new HomePageObject(driver);
    }

    public static RegisterPageObject getRegisterPage(WebDriver driver){
        return new RegisterPageObject(driver);
    }

    public static MyDashBoardPageObject getDashBoardPage(WebDriver driver){
        return new MyDashBoardPageObject(driver);
    }

    public static LoginPageObject getLoginPage(WebDriver driver){
        return new LoginPageObject(driver);
    }
}
