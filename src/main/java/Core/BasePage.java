package Core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    private WebElement element;
    private WebDriverWait webDriverWait;
    private int time = 30;


    protected void waitUntilElemenatVisible(WebDriver driver, String element){
        webDriverWait = new WebDriverWait(driver, time);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(byXpath(element)));
    }

    protected void waitUntilElementClickAble(WebDriver driver, String xpath){
        webDriverWait = new WebDriverWait(driver, time);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(byXpath(xpath)));
    }

    private By byXpath(String locator){
        return By.xpath(locator);
    }

    private WebElement findElement(WebDriver driver, String xpath){
        WebElement element = driver.findElement(byXpath(xpath));
        return  element;
    }

    protected void inputText(WebDriver driver, String xpath, String text){
        element = findElement(driver, xpath);
        element.clear();
        element.sendKeys(text);
    }

    protected void clickButton(WebDriver driver, String xpath){
        findElement(driver,xpath).click();
    }

    protected boolean isDisplay(WebDriver driver, String xpath){
        return findElement(driver, xpath).isDisplayed();
    }
}
