package TestCase;

import Core.BaseTest;
import PageObject.HomePageObject;
import PageObject.LoginPageObject;
import PageObject.MyDashBoardPageObject;
import PageObject.RegisterPageObject;
import Util.DataTest;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;

public class TestCaseSeleniumGrid extends BaseTest{

    DataTest data = DataTest.getData();

    private final String firstName = data.getFirstName();
    private final String lastName = data.getLastName();
    private final String email = data.getEmail();
    private final String password = "123456";

    private WebDriver driver;
    private HomePageObject homePageObject;
    private RegisterPageObject registerPageObject;
    private MyDashBoardPageObject myDashBoardPageObject;
    private LoginPageObject loginPageObject;

    @Parameters({"browser", "url", "ip", "port"})
    @BeforeMethod
    public void BeforMethod(String browser, String url, String ip, String port) throws MalformedURLException {
        driver = getDriverGrid(browser, url, ip, port);
    }

    @AfterMethod
    public void AfterMethod(){
        driver.close();
    }

    @Test(priority = 1)
    public void Register_Account(){
        homePageObject = new HomePageObject(driver);
        registerPageObject = homePageObject.OpenRegisterPage();
        registerPageObject.inputFirstName(firstName);
        registerPageObject.inputLastName(lastName);
        registerPageObject.inputEmail(email);
        registerPageObject.inputPassword(password);
        registerPageObject.inputConfirmPassword(password);
        myDashBoardPageObject = registerPageObject.clickRegisterButton();
        myDashBoardPageObject.CheckPageTitleDisplays();
    }

    @Test(priority = 2)
    public void Login_Account(){
        homePageObject = new HomePageObject(driver);
        loginPageObject = homePageObject.OpenLoginPage();
        myDashBoardPageObject = loginPageObject.login(email, password);
        myDashBoardPageObject.CheckPageTitleDisplays();
    }
}
